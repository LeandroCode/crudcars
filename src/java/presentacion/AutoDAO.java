
package presentacion;
/**
 * These are all the libraries used in this class.
 */

import java.sql.*;
import java.util.ArrayList;
import java.util.TreeMap;
/**
 * 
 * @author Leandro Villarreal <leavilla80@gmail.com>
 */
public class AutoDAO extends AdministradorDeConexiones{
    /**
     * 
     * @return. This variable returns all data table from the database.   
     */
    public static ArrayList<TreeMap> Listado(){
        //New instance of arraylist that stores all the rows of the table 'autos'.
        ArrayList<TreeMap> listado = new ArrayList();
        
        try {
            //We obtain the connection through the method and assign it to a variable.
            Connection conexion = AdministradorDeConexiones.getConnection();
            //It prepares the query and stores it to a variable of type PrepareStatement.
            PreparedStatement sentencia = conexion.prepareStatement("Select * from autos");
            //It executes the query and stores it in a variable called result.
            ResultSet resultado = sentencia.executeQuery();
            //Condition used to ask if there is more data in the table
            //so The next method moves the cursor to the next row.
             while(resultado.next()){
                //Creates treemap type object. 
                TreeMap autoActual = new TreeMap();
                //Assigns values provenient from table 'autos' to treemap type object.
                autoActual.put("auto_id", resultado.getString("au_id"));
                autoActual.put("auto_marca", resultado.getString("au_marca"));
                autoActual.put("auto_modelo", resultado.getString("au_modelo"));
                autoActual.put("auto_precio", resultado.getString("au_precio"));
                autoActual.put("auto_color", resultado.getString("au_color"));
                
                //Collects treemap type object with all the rows from table 'autos'.
                listado.add(autoActual);
            }
             //Execute the query and then close the connection.
              resultado.close();
              sentencia.close();
              conexion.close();
         //Connection error.     
        } catch (Exception ex) {
            System.err.println("SQLException: " + ex.getMessage());
           return null;
        }
        //It contains and returns all data from table 'autos'.
      return listado;
    }
    /**
     * 
     * @param Indice. it is a specific key entered by the user that returns a specific row from table 'autos'.
     * @return 
     */
    public static ArrayList<TreeMap> BuscarXindice(Integer Indice){
            //New instance of arraylist
            ArrayList<TreeMap> listaXId = new ArrayList();
        try {
            //We obtain the connection through the method and assign it to a variable.
            Connection conexion = AdministradorDeConexiones.getConnection();
            //It prepares the query and stores it to a variable of type PrepareStatement.
            PreparedStatement sentencia = conexion.prepareStatement("Select * from autos where(au_id = '" + Indice + "%')");
            //It executes the query and stores it in a variable called result.
            ResultSet resultado = sentencia.executeQuery();
            //Condition used to ask if there is more data in the table
            //so The next method moves the cursor to the next row.
            while(resultado.next()){
                //Creates treemap type object.
                TreeMap autoActual = new TreeMap();
                //Assigns values provenient from table 'autos' to treemap type object.
                autoActual.put("auto_id", resultado.getString("au_id"));
                autoActual.put("auto_marca", resultado.getString("au_marca"));
                autoActual.put("auto_modelo", resultado.getString("au_modelo"));
                autoActual.put("auto_precio", resultado.getString("au_precio"));
                autoActual.put("auto_color", resultado.getString("au_color"));
                
               //Collects treemap type object with the specific row from table 'autos'.
                listaXId.add(autoActual);
            }
            //Execute the query and then close the connection.
            resultado.close();
            sentencia.close();
            conexion.close();
            //Connection error.
        } catch (Exception ex) {
            System.err.println("SQLException: " + ex.getMessage());
           return null;
        }
        //It contains and returns the specific row from table 'autos'.
         return listaXId;
    }
   
}
