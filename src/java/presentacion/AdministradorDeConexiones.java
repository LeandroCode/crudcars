/*
 * AdministradorDeConexiones.java
 *
 *
 */

package presentacion;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author Leandro Villarreal <leavilla80@gmail.com>
 */
public abstract class AdministradorDeConexiones {
    
    /**
     * Creates a new instance of AdministradorDeConexiones
     */
    public AdministradorDeConexiones() {
    }
    /**
     * 
     * @return. this variable returns the connection.
     * @throws Exception. this occurs when the connection is not successful.
     */
    public static Connection getConnection() throws Exception
    {

        // It sets the name of the driver to use.
        String dbDriver = "com.mysql.jdbc.Driver";
        
        // It sets the connection to use against the database.
        String dbConnString = "jdbc:mysql://localhost/j2se";
        
        // It sets the database user.
        String dbUser = "root";
        
        // It sets the database password.
        String dbPassword = "";
        
        // Set the connection driver.
        Class.forName(dbDriver).newInstance();
        
        // It returns the connection.
        return DriverManager.getConnection(dbConnString, dbUser, dbPassword);
    }    
    
}
