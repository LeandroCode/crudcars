package presentacion;
/**
 * These are all the libraries used in this class.
 */
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This Servlet Pretends to Collect, Process and Return Information to the Navigation Website. 
 * @author Leandro
 */ 

@WebServlet(name = "AutosXID", urlPatterns = {"/AutosXID"})
public class AutosXID extends HttpServlet {
/**
 * 
 * @param request. This param receives and processes information that client has just sent from the website.   
 * @param response. This object returns information that has been processed through the request object before.
 * @throws ServletException.This extends from java.lang.Exception and is thrown by a servlet
 *                          when a servlet related problem occurs.
 * @throws IOException. This extends from java.lang.Exception and is thrown by a servlet
 *                          when a servlet related problem occurs.
 */ 
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //This method sets the content type that will be used to return information to the client.
        response.setContentType("text/html;charset=UTF-8");
        //It returns a PrintWriter object that can send character text to the client.
        PrintWriter out = response.getWriter();
        //It constructs a gson object.
        Gson convertir = new Gson();
        //It stores a string with the content of the parameter. 
        String filtro = request.getParameter("filtroID");
        
        
        //This condition evaluates if the parameter is null or not and if the specified key is positive.
        if (filtro != null && filtro.length() > 0) {
            //The specified key is stored in a variable.
            String elvalor = filtro;
            //We look for the specific row through the value entered by the user and store it in a variable. 
            ArrayList<TreeMap> valorFinal = AutoDAO.BuscarXindice(Integer.parseInt(elvalor));
            //This condition evaluates if the variable contains information.
            if (valorFinal != null) {
                //It converts the value to json format and displays it to the navigation website.
                out.print(convertir.toJson(valorFinal));
            }

        } else {
            //This variable stores all the rows in the table.
            ArrayList<TreeMap> listado = AutoDAO.Listado();
            //It evaluates if the variable is not null. 
            if (listado != null) {
                //The whole table is converted to JSON and stored in a variable.
                String resultado = convertir.toJson(listado);
                //We send the result to the client
                out.println("" + resultado);
            }
        }
    }
/**
 * 
 * @param req. This param receives and processes information that client has just sent from the website.
 * @param resp. This object converts information to text.
 * @throws ServletException.This extends from java.lang.Exception and is thrown by a servlet
 *                          when a servlet related problem occurs.
 * @throws IOException. This extends from java.lang.Exception and is thrown by a servlet
 *                          when a servlet related problem occurs. 
 */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        //It sets the response format in text
        resp.setContentType("text/html;charset=UTF-8");
        //This variable stores the reading of the data entered by the user.
        BufferedReader reader = req.getReader();
        //It constructs a gson object.
        Gson convertir = new Gson();
        //It returns a treemap with the content of the variable whose name is 'reader'.
        TreeMap<String, String> param_auto = convertir.fromJson(reader, TreeMap.class);
        //It stores the values obtained from the database in a string.
        String marca = param_auto.get("marca");
        String modelo = param_auto.get("modelo");
        String precio = param_auto.get("precio");
        String color = param_auto.get("color");
        

        try {
            //We obtain the connection through the method and assign it to a variable.
            Connection conexion = AdministradorDeConexiones.getConnection();
            //It generates the query and stores it to a variable of type PrepareStatement.
            PreparedStatement sentencia = conexion.prepareStatement("INSERT into autos (au_id,au_marca,au_modelo,au_precio,au_color)"
                    + " VALUES (null,?,?,?,?)");
            //It sets each variable submitted by the user from the website to the database. 
            sentencia.setString(1, marca);
            sentencia.setString(2, modelo);
            sentencia.setString(3, precio);
            sentencia.setString(4, color);
            
            //Execute the query and then close the connection.
            sentencia.executeUpdate();
            sentencia.close();
            conexion.close();
            //Connection error
        } catch (Exception ex) {
            System.err.println("SQLException: " + ex.getMessage());

        }
    }
/**
 * 
 * @param req. This param receives and processes information that client has just sent from the website.
 * @param resp. This object sets the response format in text.
 * @throws ServletException.This extends from java.lang.Exception and is thrown by a servlet
 *                          when a servlet related problem occurs.
 * @throws IOException. This extends from java.lang.Exception and is thrown by a servlet
 *                      when a servlet related problem occurs. 
 */
    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        //It sets the response format in text
        resp.setContentType("text/html;charset=UTF-8");
        //This variable stores the reading of the data entered by the user.
        BufferedReader reader = req.getReader();
        //It constructs a gson object.
        Gson convertir = new Gson();
        //It returns a treemap with the content of the variable whose name is 'reader'.
        TreeMap<String, String> param_auto = convertir.fromJson(reader, TreeMap.class);
        //It stores the values obtained from the database in a string.
        String idAuto = param_auto.get("getIDx");
        String marca = param_auto.get("marcax");
        String modelo = param_auto.get("modelox");
        String precio = param_auto.get("preciox");
        String color = param_auto.get("colorx");
        

        try {
            //We obtain the connection through the method and assign it to a variable.
            Connection conexion = AdministradorDeConexiones.getConnection();
            //It generates the query and stores it to a variable of type PrepareStatement.
            PreparedStatement sentencia = conexion.prepareStatement("UPDATE autos SET au_marca = ?,au_modelo = ?,au_precio = ?,au_color = ?"
                    + "WHERE au_id = ?;");
            //It sets each variable submitted by the user from the website to the database.
            sentencia.setString(1, marca);
            sentencia.setString(2, modelo);
            sentencia.setString(3, precio);
            sentencia.setString(4, color);
            sentencia.setInt(5, Integer.parseInt(idAuto));
            //Execute the query and then close the connection.
            sentencia.executeUpdate();
            sentencia.close();
            conexion.close();
            //Connection error.
        } catch (Exception ex) {
            System.err.println("SQLException: " + ex.getMessage());

        }
    }
/**
 * 
 * @param req. catch the parameter requested from the url.
 * @param resp. This object sets the response format in text.
 * @throws ServletException This extends from java.lang.Exception and is thrown by a servlet
 *                          when a servlet related problem occurs.
 * @throws IOException This extends from java.lang.Exception and is thrown by a servlet
 *                          when a servlet related problem occurs.
 */
    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        //It sets the response format in text
        resp.setContentType("text/html;charset=UTF-8");
        //It constructs a gson object.
        Gson convertir = new Gson();
        //It stores a string with the content of the parameter.
        TreeMap<String, String> param_auto = convertir.fromJson(req.getParameter("q"), TreeMap.class);
        //The specified key is stored in a string.
        String idElim = param_auto.get("idAutox");
        
        try {
            //We obtain the connection through the method and assign it to a variable.
            Connection conexion = AdministradorDeConexiones.getConnection();
            //It generates the query and stores it to a variable of type PrepareStatement.
            PreparedStatement sentencia = conexion.prepareStatement("DELETE FROM autos WHERE au_id = ?");
            //the index to be deleted is setted in the database.
            sentencia.setInt(1, Integer.parseInt(idElim));
            //Execute the query and then close the connection.
            sentencia.executeUpdate();
            System.out.println("!!!Borrado exitoso!!!");
            sentencia.close();
            conexion.close();
            //Connection error.
        } catch (Exception ex) {
            System.err.println("SQLException: " + ex.getMessage());

        }
    }

}
