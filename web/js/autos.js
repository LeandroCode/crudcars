


var AUTO = {};
AUTO.consultar = function(comando){
    //this empty string clears the result field
    document.querySelector(comando.panelresultado).innerHTML = "";
    
    var elobjeto = {};
    //this property captures the value entered in the html input.
    elobjeto.filtroID = document.querySelector('#texto').value; 
    //new instance of xmlhttprequest object.
    var xhr=new XMLHttpRequest();
    xhr.open("GET",comando.laurl + "filtroID=" + elobjeto.filtroID);
    
    xhr.onreadystatechange = function(){
     if(xhr.readyState===4 && xhr.status===200){
        var eldato = {};
        eldato.listaAutos = JSON.parse(xhr.responseText);
        var lavista = document.querySelector(comando.laplantilla).innerHTML;
        var resultado = Mustache.render(lavista,eldato);  
        document.querySelector(comando.panelresultado).innerHTML +=resultado;
     }   
    };
    xhr.send();
    limpiarCampo();
};

AUTO.insertar = function(){
      
    var xhr=new XMLHttpRequest();
    var elobjeto = {};
    xhr.open("POST","AutosXID");
    
    elobjeto.marca = document.querySelector('#marca').value; 
    elobjeto.modelo = document.querySelector('#modelo').value; 
    elobjeto.precio = document.querySelector('#precio').value; 
    elobjeto.color = document.querySelector('#color').value; 
     
   
    var textParam = JSON.stringify(elobjeto);
    
    xhr.onreadystatechange = function(){
        if( xhr.readyState === 4 && xhr.status === 200){ 
           console.log("do something"); 
           AUTO.consultar({'laurl':'AutosXID?&','laplantilla':'#laplantilla2','panelresultado':'#traerResultado2'});
        } 
    };
    
    xhr.send(textParam);

    
};
    
AUTO.actualizar = function(){
    limpiarCampos();   
    var xhr=new XMLHttpRequest();
    var elobjeto = {};
    xhr.open("PUT","AutosXID");
    
    elobjeto.getIDx = document.querySelector('#idAuto').value; 
    elobjeto.marcax = document.querySelector('#marca1').value; 
    elobjeto.modelox = document.querySelector('#modelo1').value; 
    elobjeto.preciox = document.querySelector('#precio1').value; 
    elobjeto.colorx = document.querySelector('#color1').value; 
    
    
    var textPaq = JSON.stringify(elobjeto);
    
    xhr.onreadystatechange = function(){
        if( xhr.readyState === 4 && xhr.status === 200){ 
           console.log("do something"); 
           AUTO.consultar({'laurl':'AutosXID?&','laplantilla':'#laplantilla2','panelresultado':'#traerResultado2'});
        } 
    };
    
    xhr.send(textPaq);
    limpiarCampos2();
};
AUTO.borrar = function(){
     
    var elobjeto = {};
    elobjeto.idAutox = document.querySelector('#idDelete').value;
    
    var xhr=new XMLHttpRequest();
    
    xhr.open("DELETE","AutosXID?&q=" + JSON.stringify(elobjeto));
  
    xhr.onreadystatechange = function(){
        if( xhr.readyState === 4 && xhr.status === 200){ 
           console.log("do something"); 
           AUTO.consultar({'laurl':'AutosXID?&','laplantilla':'#laplantilla2','panelresultado':'#traerResultado2'});
        } 
    }; 
        
    xhr.send();
    limpiarCampo3();
};

AUTO.inicializar = function(){
   //The setAttribute() method executes a function when a button is clicked.
    document.querySelector("#elboton").setAttribute("onclick","AUTO.insertar()"); 
    document.querySelector("#botonUpdate").setAttribute("onclick","AUTO.actualizar()"); 
    document.querySelector("#botonDELETE").setAttribute("onclick","AUTO.borrar()"); 
    document.querySelector("#elbotonaso").setAttribute("onclick","AUTO.consultar({\n\
    'laurl':'AutosXID?&',\n\
    'laplantilla':'#laplantilla2',\n\
    'panelresultado':'#traerResultado2'});");
    
};
AUTO.inicializar();

function limpiarCampo(){
    document.querySelector('#texto').value = "";
}
function limpiarCampos(){
    document.querySelector('#marca').value = "";
    document.querySelector('#modelo').value = "";
    document.querySelector('#precio').value = "";
    document.querySelector('#color').value = "";
}
function limpiarCampos2(){
    document.querySelector('#idAuto').value = "";
    document.querySelector('#marca1').value = "";
    document.querySelector('#modelo1').value = "";
    document.querySelector('#precio1').value = "";
    document.querySelector('#color1').value = "";
}
function limpiarCampo3(){
    document.querySelector('#idDelete').value = "";
}

